<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/** 
 * route resource post
 */
Route::apiResource('/post', 'PostController');

/** 
 * route resource role
 */
Route::apiResource('/role', 'RoleController');

/** 
 * route resource comment
 */
Route::apiResource('/comment', 'CommentController');

/** 
 * route resource register
 */
Route::post('/register', 'RegisterController@register');
Route::post('/login', 'LoginController@login');
Route::get('/user', 'UserController@user');

//
//
Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    // Route::post('login', 'AuthAPIController@login');
    Route::post('logout', 'AuthAPIController@logout');
    Route::post('refresh', 'AuthAPIController@refresh');
    Route::post('me', 'AuthAPIController@me');

});