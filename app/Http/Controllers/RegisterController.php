<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Resources\User as UserResource;
use App\Http\Requests\RegisterRequest;


class RegisterController extends Controller
{   
    public function register(RegisterRequest $request)
    {
        $user = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        $credentials = $request->only('email', 'password');

        // Generate a token for the user if the credentials are valid
        $token = auth()->attempt($credentials);

        return (new UserResource($request->user()))
                ->additional(['meta' => [
                    'token' => $token,
                ]]);
    }
    // /**
    //  * index
    //  *
    //  * @return void
    //  */
    // public function index()
    // {
    //     //get data from table Users
    //     $Users = User::latest()->get();

    //     //make response JSON
    //     return response()->json([ 
    //         'success' => true,
    //         'message' => 'List Data User',
    //         'data'    => $Users  
    //     ], 200);

    // }
    
    //  /**
    //  * show
    //  *
    //  * @param  mixed $id
    //  * @return void
    //  */
    // public function show($id)
    // {
    //     //find User by ID
    //     $User = User::findOrfail($id);

    //     //make response JSON
    //     return response()->json([
    //         'success' => true,
    //         'message' => 'Detail Data User',
    //         'data'    => $User  
    //     ], 200);

    // }
    
    // /**
    //  * store
    //  *
    //  * @param  mixed $request
    //  * @return void
    //  */
    // public function store(Request $request)
    // {
    //     //set validation
    //     $validator = Validator::make($request->all(), [
    //         'name' => ['string', 'required'],
    //         'username' => ['alpha_num', 'required', 'min:3', 'max:25', 'unique:users,username'],
    //         'email' => ['email', 'required', 'unique:users,username'],
    //         'password' => ['required', 'min:6']
    //     ]);
        
    //     //response error validation
    //     if ($validator->fails()) {
    //         return response()->json($validator->errors(), 400);
    //     }

    //     //save to database
    //     $User = User::create([
    //         'name'         => $request->name,
    //         'username'     => $request->username,
    //         'email'        => $request->email,
    //         'password'   => $request->password
    //     ]);

    //     //success save to database
    //     if($User) {

    //         return response()->json([
    //             'success' => true,
    //             'message' => 'User Created',
    //             'data'    => $User
    //         ], 201);

    //     } 

    //     //failed save to database
    //     return response()->json([
    //         'success' => false,
    //         'message' => 'User Failed to Created',
    //     ], 409);

    // }
    
    // /**
    //  * update
    //  **
    //  * @param  mixed $request
    //  * @param  mixed $User
    //  * @return void
    //  */
    // public function update(Request $request, User $User)
    // {
    //     //set validation
    //     $validator = Validator::make($request->all(), [
    //         'name' => ['string', 'required'],
    //         'username' => ['alpha_num', 'required', 'min:3', 'max:25', 'unique:users,username'],
    //         'email' => ['email', 'required', 'unique:users,username'],
    //         'password' => ['required', 'min:6']
    //     ]);
        
    //     //response error validation
    //     if ($validator->fails()) {
    //         return response()->json($validator->errors(), 400);
    //     }

    //     //find User by ID
    //     $User = User::findOrFail($User->id);

    //     if($User) {

    //         //update User
    //         $User->update([
    //             'name'         => $request->name,
    //             'username'     => $request->username,
    //             'email'        => $request->email,
    //             'password'   => $request->password
    //         ]);

    //         return response()->json([
    //             'success' => true,
    //             'message' => 'User Updated',
    //             'data'    => $User  
    //         ], 200);

    //     }

    //     //data User not found
    //     return response()->json([
    //         'success' => false,
    //         'message' => 'User Not Found',
    //     ], 404);

    // }
    
    // /**
    //  * destroy
    //  *
    //  * @param  mixed $id
    //  * @return void
    //  */
    // public function destroy($id)
    // {
    //     //find User by ID
    //     $User = User::findOrfail($id);

    //     if($User) {

    //         //delete User
    //         $User->delete();

    //         return response()->json([
    //             'success' => true,
    //             'message' => 'User Deleted',
    //         ], 200);

    //     }

    //     //data User not found
    //     return response()->json([
    //         'success' => false,
    //         'message' => 'User Not Found',
    //     ], 404);
    // }
}

// use Illuminate\Support\Facades\Hash;
// use Illuminate\Support\Str;

// /**
//  * Create a new user instance after a valid registration.
//  *
//  * @param  array  $data
//  * @return \App\User
//  */
// public function create(array $data)
// {
//     return User::forceCreate([
//         'name' => $data['name'],
//         'email' => $data['email'],
//         'password' => Hash::make($data['password']),
//         'api_token' => Str::random(80),
//     ]);
// }