<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{    
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table Comments
        $Comments = Comment::latest()->get();

        //make response JSON
        return response()->json([ 
            'success' => true,
            'message' => 'List Data Comment',
            'data'    => $Comments  
        ], 200);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find Comment by ID
        $Comment = Comment::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Comment',
            'data'    => $Comment  
        ], 200);

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'description' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user=auth()->user()

        //save to database
        $Comment = Comment::create([
            'content'     => $request->content,
            'description'   => $request->description,
            'user_id'   => $user->id
        ]);

        //success save to database
        if($Comment) {

            return response()->json([
                'success' => true,
                'message' => 'Comment Created',
                'data'    => $Comment  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Comment Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $Comment
     * @return void
     */
    public function update(Request $request, Comment $Comment)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'description' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find Comment by ID
        $Comment = Comment::findOrFail($Comment->id);

        if($Comment) {

            //update Comment
            $Comment->update([
                'content'     => $request->content,
                'description'   => $request->description
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Comment Updated',
                'data'    => $Comment  
            ], 200);

        }

        //data Comment not found
        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find Comment by ID
        $Comment = Comment::findOrfail($id);

        if($Comment) {

            //delete Comment
            $Comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Comment Deleted',
            ], 200);

        }

        //data Comment not found
        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }
}