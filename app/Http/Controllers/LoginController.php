<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\User as UserResource;
use App\Http\Requests\LoginRequest;

class LoginController extends Controller
{
    public function login (Request $request)
    {
        $credentials = $request->only('email', 'password');

        // Generate a token for the user if the credentials are valid
        if (!$token = auth()->attempt($credentials)){
            return response()->json(['error' => 'invalid_credentials'], 401);
        };

        return (new UserResource($request->user()))
                ->additional(['meta' => [
                    'token' => $token,
                ]]);
    }
}
