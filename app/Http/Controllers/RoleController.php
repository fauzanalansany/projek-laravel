<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{    
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table Roles
        $Roles = Role::latest()->get();

        //make response JSON
        return response()->json([ 
            'success' => true,
            'message' => 'List Data Role',
            'data'    => $Roles  
        ], 200);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find Role by ID
        $Role = Role::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Role',
            'data'    => $Role  
        ], 200);

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'nama'   => 'required',
            'description' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $Role = Role::create([
            'nama'     => $request->nama,
            'description'   => $request->description
        ]);

        //success save to database
        if($Role) {

            return response()->json([
                'success' => true,
                'message' => 'Role Created',
                'data'    => $Role  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Role Failed to Save',
        ], 409);

    }
    
    /**
     * update
     **
     * @param  mixed $request
     * @param  mixed $Role
     * @return void
     */
    public function update(Request $request, Role $Role)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'nama'   => 'required',
            'description' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find Role by ID
        $Role = Role::findOrFail($Role->id);

        if($Role) {

            //update Role
            $Role->update([
                'nama'     => $request->nama,
                'description'   => $request->description
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Role Updated',
                'data'    => $Role  
            ], 200);

        }

        //data Role not found
        return response()->json([
            'success' => false,
            'message' => 'Role Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find Role by ID
        $Role = Role::findOrfail($id);

        if($Role) {

            //delete Role
            $Role->delete();

            return response()->json([
                'success' => true,
                'message' => 'Role Deleted',
            ], 200);

        }

        //data Role not found
        return response()->json([
            'success' => false,
            'message' => 'Role Not Found',
        ], 404);
    }
}